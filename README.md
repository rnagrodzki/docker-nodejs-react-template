# Docker Nodejs React Template

Example template, how to dockerize monorepo apps and services.

### Technological stack
1. Package manager: `yarn`
2. Bundler: `Vite`

## Bootstrap

To bootstrap project run in repository root:<br/>
`$ ./bootstrap.sh`

## Yarn 3+
Not required if bootstrap script was executed!<br/>
Make sure the `Yarn 3+` is enabled by running: <br/>
* Verify version: `$ yarn -v`
* Install latest: `$ yarn set version 3.4.1`
* More information can be found here: [Yarn installation](https://yarnpkg.com/getting-started/install)

## Docker compose

To run all modules execute (in repo root):<br/>
`$ docker compose -p dnrt --env-file ./.env.local -f ./docker-compose.yml up`

To run all modules in development mode execute (in repo root):<br/>
`$ docker compose -p dnrt --env-file ./.env.dev.local -f ./docker-compose.dev.yml up`

## Docker

Please consider the section as an extra content. `docker compose` is preferable way of running the setup.<br/> 
If you would like to play with a separate containers anyway, please follow this section.

### Frontend
Build docker image:<br/>
`$ docker build -t app-test -f ./apps/spa-front/Dockerfile .`

Run container:<br/>
```
$ docker run --rm -t \
    --name spa-app \
    -p 8080:80 \
    app-test
```
Page available under: `http://localhost:8080` (default settings)

### Backend
Build docker image:<br/>
`$ docker build -t node-srv -f ./services/main-srv/Dockerfile .`

Run container:<br/>
```
$ docker run --rm -t \
    --name node-server-api \
    -p 3000:3000 \
    node-srv
```
Service available under: `http://localhost:3000` (default settings)