import fastify from 'fastify';
import * as process from "process";

const srvPort = Number.parseInt(process.env.SERVER_PORT || '');
if(Number.isNaN(srvPort) || srvPort <= 0) {
	console.error('SERVER_PORT not defined');
	process.exit(1);
}

const app = async () => {
	const app = fastify();

	app.get('/', (_req, reply) => {
		reply.send({hello: 'World'});
	});

	app.get('/ping', (_req, reply) => {
		reply.send({ msg: 'pong' });
	});

	app.get('/pong', (_req, reply) => {
		reply.send({ msg: 'ping' });
	});

	app.listen({ port: srvPort, host: '0.0.0.0' });
	return app;
};

export const mainSrv = app();